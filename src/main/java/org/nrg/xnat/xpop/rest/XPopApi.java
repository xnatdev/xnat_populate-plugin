package org.nrg.xnat.xpop.rest;

import io.swagger.annotations.*;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnat.xpop.XPopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Api(description = "XNAT Test Logger API")
@XapiRestController
@RequestMapping(value = "/testlog")
public class XPopApi extends AbstractXapiRestController {

    private final XPopService xPopService;

    @Autowired
    public XPopApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder, final XPopService xPopService) {
        super(userManagementService, roleHolder);
        this.xPopService = xPopService;
    }

    @ApiOperation(value = "Installs XNAT Populate script. NOTE: does not install groovy: you will need to that on your own.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Install successful."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Only site admins may access this resource."),
            @ApiResponse(code = 500, message = "Unexpected error.")
    })
    @XapiRequestMapping(value = "install/{version}", method = RequestMethod.POST, restrictTo = AccessLevel.Admin)
    public ResponseEntity<String> install(@ApiParam("Version of XNAT Populate to install.") @PathVariable final String version) {
        try {
            xPopService.install(version);
            return new ResponseEntity<>("XNAT Populate installed.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error in installing XNAT Populate:\n" + e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Populates XNAT with data.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Populate complete."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Only site admins may access this resource."),
            @ApiResponse(code = 500, message = "Unexpected error.")
    })
    @XapiRequestMapping(value = "populate", method = RequestMethod.PUT, restrictTo = AccessLevel.Admin)
    public ResponseEntity<String> populate(@ApiParam("The values to be passed to XNAT Populate.") @RequestBody final Map<String, String> values) {
        final String user = values.get("user");
        final String password = values.get("password");
        final String projectString = values.get("projects");
        final String siteConfig = values.get("siteConfig");

        if (user == null) {
            return new ResponseEntity<>("user must be specified in populate JSON", HttpStatus.UNPROCESSABLE_ENTITY);
        } else if (password == null) {
            return new ResponseEntity<>("password must be specified in populate JSON", HttpStatus.UNPROCESSABLE_ENTITY);
        } else if (projectString == null) {
            return new ResponseEntity<>("projects must be specified in populate JSON", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        String[] projects = projectString.split(",");
        try {
            xPopService.populate(user, password, Arrays.asList(projects), siteConfig);
            return new ResponseEntity<>("XNAT Populate run complete", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error in running XPOP:\n" + e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    private void pack(final Path folder, final Path zipFilePath) throws IOException {
        // from http://stackoverflow.com/a/35158142/5128397
        try (
                FileOutputStream fos = new FileOutputStream(zipFilePath.toFile());
                ZipOutputStream zos = new ZipOutputStream(fos)
        ) {
            Files.walkFileTree(folder, new SimpleFileVisitor<Path>() {
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    zos.putNextEntry(new ZipEntry(folder.relativize(file).toString()));
                    Files.copy(file, zos);
                    zos.closeEntry();
                    return FileVisitResult.CONTINUE;
                }

                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    zos.putNextEntry(new ZipEntry(folder.relativize(dir).toString() + "/"));
                    zos.closeEntry();
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }

    private ResponseEntity<InputStreamResource> zipResponse(Path zippedFile, String downloadedName) throws FileNotFoundException {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(zippedFile.toFile().length());
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.set("content-disposition", "attachment; filename=" + downloadedName);
        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(new FileInputStream(zippedFile.toFile())));
    }

}
