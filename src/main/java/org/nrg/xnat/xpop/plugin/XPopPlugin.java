package org.nrg.xnat.xpop.plugin;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "xnatPopulatePlugin", name = "XNAT Populate Plugin",
        description = "This plugin provides a UI and REST API to run the XNAT Populate script within an XNAT. Use only in dev environments without sensitive data.")
@ComponentScan({"org.nrg.xnat.xpop.rest", "org.nrg.xnat.xpop"})
public class XPopPlugin {
}
