package org.nrg.xnat.xpop;

import org.nrg.xdat.XDAT;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class XPopService {

    private Path home = Paths.get(System.getProperty("user.home"));
    private Path xpopHome = home.resolve("xnat_populate");

    public void install(String version) throws IOException, InterruptedException {
        final Process installProc;
        final ProcessBuilder installBuilder = new ProcessBuilder("git", "clone", "git@bitbucket.org:xnatdev/xnat_populate.git");
        installBuilder.directory(home.toFile());
        installProc = installBuilder.start();
        installProc.waitFor();
    }

    public void populate(String user, String password, List<String> projects, String siteConfig) throws Exception {
        final String projectFileName = "projectList.txt";
        final File projectFile = xpopHome.resolve(projectFileName).toFile();
        if (projectFile.exists()) {
            projectFile.delete();
        }

        FileWriter writer = new FileWriter(projectFile);
        for (String project : projects) {
            writer.write(project + "\n");
        }
        writer.close();

        final List<String> command = new ArrayList<>();
        command.add("groovy");
        command.add("PopulateXnat.groovy");
        command.add("-u");
        command.add(user);
        command.add("-p");
        command.add(password);
        command.add("-s");
        command.add(XDAT.getSiteConfigPreferences().getSiteUrl());
        command.add("-d");
        command.add(projectFileName);
        if (siteConfig != null) {
            command.add("-g");
            command.add(siteConfig);
        }

        final Process populate;
        final ProcessBuilder populateBuilder = new ProcessBuilder(command.toArray(new String[]{}));
        populateBuilder.directory(xpopHome.toFile());
        populate = populateBuilder.start();
        populate.waitFor();
    }

}
